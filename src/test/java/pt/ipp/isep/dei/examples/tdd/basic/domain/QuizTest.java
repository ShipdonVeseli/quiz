package pt.ipp.isep.dei.examples.tdd.basic.domain;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class QuizTest {

    @BeforeAll
    public static void classSetUp() {
        //HACK: for demonstration purposes only
        System.out.println(
                "This is a QuizTest class method and takes place before any @Test is executed");
    }

    @AfterAll
    public static void classTearDown() {
        //HACK: for demonstration purposes only
        System.out.println(
                "This is a QuizTest class method and takes place after all @Test are executed");
    }

    @BeforeEach
    public void setUp() {
        //HACK: for demonstration purposes only
        System.out.println(
                "\tThis call takes place before each @Test is executed");
    }

    @AfterEach
    public void tearDown() {
        //HACK: for demonstration purposes only
        System.out.println(
                "\tThis call takes place after each @Test is executed");
    }

    @Test
    @Disabled
    public void failingTest() {
        fail("a disabled failing test");
    }

    @Test
    public void MoveThreeTimesForwardAndPaintCell() {
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        Integer[][] expectedResult = new Integer[6][6];
        for(int i = 0; i < 6; i++){
            for(int j = 0; j < 6; j++){
                expectedResult[i][j] = 0;
            }
        }
        expectedResult[0][3] = 1;

        Quiz quiz = new Quiz();
        quiz.runInstruction("fffp");
        Integer[][] result = quiz.returnField();

        assertArrayEquals(expectedResult, result);
    }

    @Test
    public void FirstColumnSet() {
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");
        Quiz quiz = new Quiz();

        Integer[][] expectedResult = new Integer[6][6];
        quiz.initField(expectedResult);
        expectedResult[0][0] = 1;
        expectedResult[1][0] = 1;
        expectedResult[2][0] = 1;
        expectedResult[3][0] = 1;
        expectedResult[4][0] = 1;
        expectedResult[5][0] = 1;


        quiz.runInstruction("rpfpfpfpfpfp");
        Integer[][] result = quiz.returnField();

        assertArrayEquals(expectedResult, result);
    }

    @Test
    public void MiddleColumSet() {
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");
        Quiz quiz = new Quiz();
        Integer[][] expectedResult = new Integer[6][6];
        quiz.initField(expectedResult);

        expectedResult[0][2] = 1;
        expectedResult[1][2] = 1;
        expectedResult[2][2] = 1;
        expectedResult[3][2] = 1;
        expectedResult[4][2] = 1;
        expectedResult[5][2] = 1;

        quiz.runInstruction("ffrpfpfpfpfpfp");
        Integer[][] result = quiz.returnField();

        assertArrayEquals(expectedResult, result);
    }

    @Test
    public void LastRowSet() {
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        Quiz quiz = new Quiz();
        Integer[][] expectedResult = new Integer[6][6];
        quiz.initField(expectedResult);

        expectedResult[5][0] = 1;
        expectedResult[5][1] = 1;
        expectedResult[5][2] = 1;
        expectedResult[5][3] = 1;
        expectedResult[5][4] = 1;
        expectedResult[5][5] = 1;

        quiz.runInstruction("rfffffrrrpfpfpfpfpfp");
        Integer[][] result = quiz.returnField();

        assertArrayEquals(expectedResult, result);
    }

    @Test
    public void MiddleRowSet() {
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        Quiz quiz = new Quiz();
        Integer[][] expectedResult = new Integer[6][6];
        quiz.initField(expectedResult);

        expectedResult[2][0] = 1;
        expectedResult[2][1] = 1;
        expectedResult[2][2] = 1;
        expectedResult[2][3] = 1;
        expectedResult[2][4] = 1;
        expectedResult[2][5] = 1;

        quiz.runInstruction("rffrrrpfpfpfpfpfp");
        Integer[][] result = quiz.returnField();

        assertArrayEquals(expectedResult, result);
    }

    @Test
    public void FirstColumAndLastRowSet() {
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        Quiz quiz = new Quiz();
        Integer[][] expectedResult = new Integer[6][6];
        quiz.initField(expectedResult);

        expectedResult[5][0] = 1;
        expectedResult[5][1] = 1;
        expectedResult[5][2] = 1;
        expectedResult[5][3] = 1;
        expectedResult[5][4] = 1;
        expectedResult[5][5] = 1;

        expectedResult[1][0] = 1;
        expectedResult[2][0] = 1;
        expectedResult[3][0] = 1;
        expectedResult[4][0] = 1;
        expectedResult[5][0] = 1;
        expectedResult[0][0] = 1;

        quiz.runInstruction("prfpfpfpfpfprrrfpfpfpfpfp");
        Integer[][] result = quiz.returnField();

        assertArrayEquals(expectedResult, result);
    }

    @Test
    public void BorderSet() {
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        Quiz quiz = new Quiz();
        Integer[][] expectedResult = new Integer[6][6];
        quiz.initField(expectedResult);

        expectedResult[5][0] = 1;
        expectedResult[5][1] = 1;
        expectedResult[5][2] = 1;
        expectedResult[5][3] = 1;
        expectedResult[5][4] = 1;
        expectedResult[5][5] = 1;

        expectedResult[1][0] = 1;
        expectedResult[2][0] = 1;
        expectedResult[3][0] = 1;
        expectedResult[4][0] = 1;
        expectedResult[5][0] = 1;
        expectedResult[0][0] = 1;

        expectedResult[1][5] = 1;
        expectedResult[2][5] = 1;
        expectedResult[3][5] = 1;
        expectedResult[4][5] = 1;
        expectedResult[5][5] = 1;
        expectedResult[0][5] = 1;

        expectedResult[0][0] = 1;
        expectedResult[0][1] = 1;
        expectedResult[0][2] = 1;
        expectedResult[0][3] = 1;
        expectedResult[0][4] = 1;
        expectedResult[0][5] = 1;

        quiz.runInstruction("pfpfpfpfpfprfpfpfpfpfprfpfpfpfpfprfpfpfpfp");
        Integer[][] result = quiz.returnField();

        assertArrayEquals(expectedResult, result);
    }

    @Test
    public void AllFieldSet() {
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        Integer[][] expectedResult = new Integer[6][6];
        for(int i = 0; i < 6; i++){
            for(int j = 0; j < 6; j++){
                expectedResult[i][j] = 1;
            }
        }


        Quiz quiz = new Quiz();
        quiz.runInstruction("pfpfpfpfpfprfprfpfpfpfpfprrrfprrrfpfpfpfpfprfprfpfpfpfpfprrrfprrrfpfpfpfpfprfprfpfpfpfpfp");
        Integer[][] result = quiz.returnField();

        assertArrayEquals(expectedResult, result);
    }

    @Test
    public void WrongCharacterAndOutOfField() {
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        Quiz quiz = new Quiz();
        Integer[][] expectedResult = new Integer[6][6];
        quiz.initField(expectedResult);

        expectedResult[0][0] = 1;

        quiz.runInstruction("prrfpk");
        Integer[][] result = quiz.returnField();

        assertArrayEquals(expectedResult, result);
    }
}



