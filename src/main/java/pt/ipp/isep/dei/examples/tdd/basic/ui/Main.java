package pt.ipp.isep.dei.examples.tdd.basic.ui;

import pt.ipp.isep.dei.examples.tdd.basic.domain.Quiz;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Quiz quiz = new Quiz();

        Scanner scanner = new Scanner(System.in);
        System.out.println("f = forward, r = rotate, p = paint");
        quiz.runInstruction(scanner.nextLine());
        for(int i = 0; i < 6; i++){
            for(int j = 0; j < 6; j++){
                System.out.print(quiz.returnField()[i][j]);
            }
            System.out.println("");
        }
    }
}
