package pt.ipp.isep.dei.examples.tdd.basic.domain;

public class Quiz {
    private Integer direction = 0;  //0=right 1=down 2=left 3=up
    private Integer[][] field = new Integer[6][6];
    private Integer horizontalPos = 0;
    private Integer verticalPos = 0;

    public Quiz(){
        initField(field);
    }

    public Integer[][] initField(Integer[][] field2){
        for(int i = 0; i < 6; i++){
            for(int j = 0; j < 6; j++){
                field2[i][j] = 0;
            }
        }
        return field2;
    }

    private void paintCell(){
        try {
            field[verticalPos][horizontalPos] = 1;
        }catch(java.lang.ArrayIndexOutOfBoundsException e){
            System.out.println("Out of Field");
        }
    }

    public void moveForward(){
        switch(direction){
            case 0:
                horizontalPos++;
                break;
            case 1:
                verticalPos++;
                break;
            case 2:
                horizontalPos--;
                break;
            case 3:
                verticalPos--;
                break;
        }
    }

    public void rotate(){
        direction = (direction+1) % 4;
    }

    public void runInstruction(String input){
        for(int i = 0; i < input.length(); i++){
            switch(input.charAt(i)){
                case 'f':
                    moveForward();
                    break;
                case 'r':
                    rotate();
                    break;
                case 'p':
                    paintCell();
                    break;
                default:
                    System.out.println("Error Wrong character");
                    break;
            }
        }
    }

    public Integer[][] returnField(){
        return field;
    }
}
